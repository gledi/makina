from rest_framework import serializers

from vehicles.models import Vehicle


class VehicleSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Vehicle
        fields = [
            'brand',
            'model',
            'year',
            'transmission',
            'description',
            'is_on_sale',
        ]
